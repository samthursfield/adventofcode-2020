use std::collections::HashMap;
use std::io;
use std::fs::File;
use std::io::Read;

#[derive(Clone)]
#[derive(Debug)]
struct Instruction {
    operator: String,
    argument: isize,
}

#[derive(Debug)]
struct VirtualMachine {
    program: Vec<Instruction>,
    pc: isize,
    acc: isize,
    counter: HashMap<isize, usize>,
}


impl VirtualMachine {
    fn new(program: Vec<Instruction>) -> Self {
        return Self { program, pc: 0, acc: 0, counter: HashMap::new() }
    }

    fn step(&mut self) {
        let instr = &self.program[self.pc as usize];
        //println!("Executing {:?} at pc {}", instr, self.pc);

        let count = self.counter.entry(self.pc).or_insert(0);
        *count += 1;

        match instr.operator.as_str() {
            "acc" => {
                self.acc = self.acc + instr.argument;
                self.pc += 1;
            }
            "jmp" => {
                self.pc = self.pc + instr.argument;
            }
            "nop" => {
                self.pc += 1;
            }
            _ => {
                panic!("Invalid instruction!");
            }
        }
    }

    fn run(&mut self) -> (isize, isize) {
        loop {
            self.step();

            if self.pc as usize >= self.program.len() {
                println!("Correct termination detected.");
                return (self.pc, self.acc);
            }

            if let Some(1) = self.counter.get(&self.pc) {
                println!("Loop detected at PC {}", self.pc);
                return (self.pc, self.acc);
            }
        }
    }
}

fn load_program(input: &str) -> Vec<Instruction> {
    return input.lines().map(|line| {
        let mut parts = line.split_ascii_whitespace();
        Instruction {
            operator: parts.next().unwrap().to_string(),
            argument: parts.next().unwrap().parse().unwrap(),
        }
    }).collect();
}

fn find_program_that_terminates_correctly(program: &
                                          Vec<Instruction>) -> (usize, isize) {
    for (i, instr) in program.iter().enumerate() {
        if let "nop"|"jmp" = instr.operator.as_str() {
            if instr.argument == 0 {
                println!("Ignoring 'nop 0' at {}", i);
            } else {
                println!("Modifying '{:?}' at {}", instr, i);
                let mut modified_program = program.to_vec();

                if instr.operator.as_str() == "nop" {
                    modified_program[i].operator = "jmp".to_string();
                } else {
                    modified_program[i].operator = "nop".to_string();
                }

                let mut vm = VirtualMachine::new(modified_program);
                let (pc, acc) = vm.run();
                if pc as usize >= program.len() {
                    println!("Got correct value after changing position {}", i);
                    return (i, acc);
                }
           }
        }
    }
    panic!("Didn't find the right code.");
}

#[cfg(test)]
mod test {
    use super::*;

    const TEST_PROGRAM: &str = r#"nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6"#;

    #[test]
    fn test_1() {
        let program = load_program(&TEST_PROGRAM);
        assert_eq!(program.len(), 9);

        let mut vm = VirtualMachine::new(program);
        let (pc, acc) = vm.run();
        assert_eq!(pc, 1);
        assert_eq!(acc, 5);
    }

    fn test_2() {
        let program = load_program(&TEST_PROGRAM);
        assert_eq!(program.len(), 9);

        let (pos, _) = find_program_that_terminates_correctly(&program);
        assert_eq!(pos, 8);
    }
}

fn main() -> Result<(), io::Error> {
    let mut file: File = File::open("input")?;
    let mut input = String::new();
    file.read_to_string(&mut input)?;

    let program = load_program(&input);

    let (_, acc) = find_program_that_terminates_correctly(&program);
    println!("Program terminated correctly, accumulator {}", acc);
    return Ok(());
}
