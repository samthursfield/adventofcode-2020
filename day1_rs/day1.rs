use std::io;
use std::fs::File;
use std::io::Read;
use std::str::FromStr;

fn main() -> io::Result<()> {
    let mut f = File::open("input")?;
    let mut buf = String::new();
    f.read_to_string(&mut buf)?;

    let ints: Vec<_> = buf.lines()
        .map(|s| i32::from_str(s).unwrap())
        .collect();

    for a in &ints {
        for b in &ints {
            if a + b == 2020 {
                println!("{}", a * b);
            }
        }
    }

    Ok(())
}
