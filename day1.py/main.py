ints = set()

with open('input') as f:
    for line in f:
        ints.add(int(line))

for a in ints:
    for b in ints:
        for c in ints:
            if a + b + c == 2020:
                print(f"It's {a} + {b} + {c}")
                factor = a * b * c
                print(f"Factor is {factor}")
                break
