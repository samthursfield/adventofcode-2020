use std::io;
use std::fs::File;
use std::io::Read;

fn code_iter(code: &Vec<char>, index: usize, low: i32, high: i32) -> i32 {
    if index >= code.len() {
        return low;
    } else {
        println!("  iter at {}, {}. next code {}: {:?}, {}", low, high, code[index], code, index);
        match code[index] {
            'F' => { code_iter(code, index + 1, low, low + ((high - low) / 2)) }
            'B' => { code_iter(code, index + 1, low + ((high - low) / 2) + 1, high) }
            'L' => { code_iter(code, index + 1, low, low + ((high - low) / 2)) }
            'R' => { code_iter(code, index + 1, low + ((high - low) / 2) + 1, high) }
            _ => { panic!("Invalid code") }
        }
    }
}

fn seat_id(code: &str) -> i32 {
    let chars: Vec<char> = code.chars().collect();
    let row_code: Vec<char> = chars[0..7].to_vec();
    let seat_code: Vec<char> = chars[7..10].to_vec();

    let row = code_iter(&row_code, 0, 0, 127);
    let seat = code_iter(&seat_code, 0, 0, 7);
    println!("Row {:?} -> {}, seat {:?} -> {}", row_code, row, seat_code, seat);
    return (row * 8 ) + seat;
}

mod test {
    use super::*;

    #[test]
    fn test_1() {
        assert_eq!(seat_id("FBFBBFFRLR"), 357);
        assert_eq!(seat_id("BFFFBBFRRR"), 567);
        assert_eq!(seat_id("FFFBBBFRRR"), 119);
        assert_eq!(seat_id("BBFFBBFRLL"), 820);
    }
}

fn main() -> Result<(), io::Error> {
    let mut file: File = File::open("input")?;
    let mut input = String::new();
    file.read_to_string(&mut input)?;

    let mut seats: [bool; 1024] = [false; 1024];
    let mut max_seat_id = 0;
    for line in input.lines() {
        let seat_id = seat_id(line);
        seats[seat_id as usize] = true;
        if seat_id > max_seat_id {
            max_seat_id = seat_id;
        }
    }
    println!("Max seat ID: {}", max_seat_id);
    for seat_id in 0..1024 {
        if ! seats[seat_id] {
            println!("{}", seat_id);
        }
    }
    return Ok(());
}
