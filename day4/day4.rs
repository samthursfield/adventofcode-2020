use std::collections::HashMap;
use std::collections::HashSet;
use std::io;
use std::fs::File;
use std::io::Read;

#[derive(Debug)]
struct Passport {
    fields: HashMap<String, String>
}

impl Passport {
    fn new() -> Passport {
        return Passport { fields: HashMap::new() };
    }

    const REQUIRED_FIELDS: [&'static str; 7] = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"];

    fn validate_year(&self, text: &str, min: i32, max: i32) -> bool {
        let value: i32 = text.parse().unwrap();
        text.len() == 4 && value >= min && value <= max
    }

    fn validate_height(&self, text: &str) -> bool {
        let s = String::from(text);
        let unit: &str = &s[s.len()-2..];
        let value_result: Result<i32, _> = s[0..s.len()-2].parse();
        if let Ok(value) = value_result {
            match unit {
                "cm" => { return value >= 150 && value <= 193; }
                "in" => { return value >= 59 && value <= 76; }
                _ => { false }
            }
        } else {
            return false
        }
    }

    fn validate_hex_color(&self, text: &str) -> bool {
        let s = String::from(text);
        let mut chars = s.chars();
        let first_char = chars.next();
        let rest: Vec<char> = chars.collect();
        return first_char == Some('#') && rest.iter().all(|c| c.is_digit(16));
    }

    fn validate_eye_color(&self, text: &str) -> bool {
        match text {
            "amb"|"blu"|"brn"|"gry"|"grn"|"hzl"|"oth" => { true }
            _ => { false }
        }
    }

    fn validate_passport_id(&self, text: &str) -> bool {
        let valid_int: bool = match text.parse::<i32>() {
            Ok(_) => true,
            Err(_) => false,
        };
        return text.len() == 9 && valid_int
    }

    fn validate_field(&self, fieldname: &str, value: &str) -> bool {
        println!("Validating {} = {}", fieldname, value);
        match fieldname {
            "byr" => { self.validate_year(value, 1920, 2002) }
            "iyr" => { self.validate_year(value, 2010, 2020) }
            "eyr" => { self.validate_year(value, 2020, 2030) }
            "hgt" => { self.validate_height(value) }
            "hcl" => { self.validate_hex_color(value) }
            "ecl" => { self.validate_eye_color(value) }
            "pid" => { self.validate_passport_id(value) }
            "cid" => { true }
            _ => { panic!("Unknown field"); }
        }
    }

    fn is_valid(&self) -> bool {
        let fields_present = Self::REQUIRED_FIELDS
            .iter()
            .copied()
            .all(|f| self.fields.contains_key(f));
        let fields_valid = self.fields
            .iter()
            .all(|p| self.validate_field(p.0, p.1));

        return fields_present && fields_valid;
    }
}

fn load_passports(input: &str) -> Vec<Passport> {
    let mut result: Vec<Passport> = Vec::new();
    let mut passport = Passport::new();

    for line in input.lines() {
        let fields: Vec<&str> = line.split_ascii_whitespace().collect();
        if fields.is_empty() {
            result.push(passport);
            passport = Passport::new();
        } else {
            for field in fields {
                /* There is a neater way to do this using the Itertools crate.
                 * See: https://stackoverflow.com/questions/62257213/unpack-a-splitn-into-a-tuple-in-rust */
                let mut iter = field.splitn(2, ":");
                let name = iter.next().unwrap();
                let value = iter.next().unwrap();

                passport.fields.insert(name.to_string(), value.to_string());
            }
        }
    }
    result.push(passport);

    return result;
}

fn count_valid_passports(passports: Vec<Passport>) -> usize {
    return passports
        .iter()
        .filter(|p| p.is_valid())
        .count();
}

#[cfg(test)]
mod test {
    use super::*;

    const TEST_INPUT_FIELDS: &str = r#"ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
byr:1937 iyr:2017 cid:147 hgt:183cm

iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
hcl:#cfa07d byr:1929

hcl:#ae17e1 iyr:2013
eyr:2024
ecl:brn pid:760753108 byr:1931
hgt:179cm

hcl:#cfa07d eyr:2025 pid:166559648
iyr:2011 ecl:brn hgt:59in"#;

    #[test]
    fn test_required_fields_are_checked() {
        let passports = load_passports(&TEST_INPUT_FIELDS);
        assert_eq!(passports.len(), 4);
        assert_eq!(count_valid_passports(passports), 2);
    }

    const TEST_INPUT_INVALID_VALUES: &str = r#"eyr:1972 cid:100
hcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926

iyr:2019
hcl:#602927 eyr:1967 hgt:170cm
ecl:grn pid:012533040 byr:1946

hcl:dab227 iyr:2012
ecl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277

hgt:59cm ecl:zzz
eyr:2038 hcl:74454a iyr:2023
pid:3556412378 byr:2007"#;

    #[test]
    fn test_invalid_field_values() {
        let passports = load_passports(&TEST_INPUT_INVALID_VALUES);
        assert_eq!(passports.len(), 4);
        assert_eq!(count_valid_passports(passports), 0);
    }

    const TEST_INPUT_VALID_VALUES: &str = r#"pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980
hcl:#623a2f

eyr:2029 ecl:blu cid:129 byr:1989
iyr:2014 pid:896056539 hcl:#a97842 hgt:165cm

hcl:#888785
hgt:164cm byr:2001 iyr:2015 cid:88
pid:545766238 ecl:hzl
eyr:2022

iyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719"#;

    #[test]
    fn test_valid_field_values() {
        let passports = load_passports(&TEST_INPUT_VALID_VALUES);
        assert_eq!(passports.len(), 4);
        assert_eq!(count_valid_passports(passports), 4);
    }
}

fn main() -> Result<(), io::Error> {
    let mut file: File = File::open("input")?;
    let mut input = String::new();
    file.read_to_string(&mut input)?;

    let passports = load_passports(&input);

    println!("{} passports are valid.", count_valid_passports(passports));

    return Ok(());
}
