import re


def is_valid_by_count(password, letter, min_count, max_count):
    letter_count = len([l for l in password if l == letter])
    return min_count <= letter_count and max_count >= letter_count


def is_valid_by_pos(password, letter, pos_1, pos_2):
    if password[pos_1 - 1] == letter:
        if password[pos_2 - 1] == letter:
            return False
        return True
    elif password[pos_2 - 1] == letter:
        return True
    else:
        return False


pattern = re.compile('([\d]+)-([\d]+) ([\w]): ([\w]+)')
with open('input') as f:
    for line in f:
        match = pattern.match(line)
        num1, num2, letter, password = match.groups()

        #if is_valid_by_count(password, letter, int(num1), int(num2)):
        #    print(password)

        if is_valid_by_pos(password, letter, int(num1), int(num2)):
            print(password)
