use std::collections::HashMap;
use std::io;
use std::fs::File;
use std::io::Read;

#[derive(Debug)]
struct Group {
    questions: HashMap<char, i32>,
    size: i32,
}

impl Group {
    fn new() -> Group {
        return Group { questions: HashMap::new(), size: 0 };
    }

    fn count_member(&mut self) {
        self.size += 1;
    }

    fn count_answer(&mut self, question: char) {
        if self.questions.contains_key(&question) {
            self.questions.insert(question, self.questions.get(&question).unwrap() + 1);
        } else {
            self.questions.insert(question, 1);
        }
    }

    fn questions_answered_yes_for_any(&self) -> usize {
        return self.questions.len();
    }
    fn questions_answered_yes_for_all(&self) -> usize {
        return self.questions.iter().filter(|pair| *pair.1 == self.size ).count();
    }
}

fn load_groups(input: &str) -> Vec<Group> {
    let mut result: Vec<Group> = Vec::new();
    let mut group = Group::new();

    for line in input.lines() {
        if line.is_empty() {
            result.push(group);
            group = Group::new();
        } else {
            group.count_member();
            for question in line.chars() {
                group.count_answer(question);
            }
        }
    }
    result.push(group);

    return result;
}

fn questions_yes_for_any(groups: &Vec<Group>) -> usize {
    return groups.iter().map(|g| g.questions_answered_yes_for_any()).sum::<usize>();
}

fn questions_yes_for_all(groups: &Vec<Group>) -> usize {
    return groups.iter().map(|g| g.questions_answered_yes_for_all()).sum::<usize>();
}

#[cfg(test)]
mod test {
    use super::*;

    const TEST_GROUPS: &str = r#"abc

a
b
c

ab
ac

a
a
a
a

b"#;

    #[test]
    fn test_parse() {
        let groups = load_groups(&TEST_GROUPS);
        assert_eq!(groups.len(), 5);
        assert_eq!(questions_yes_for_any(&groups), 11);
        assert_eq!(questions_yes_for_all(&groups), 6);
    }
}

fn main() -> Result<(), io::Error> {
    let mut file: File = File::open("input")?;
    let mut input = String::new();
    file.read_to_string(&mut input)?;

    let groups = load_groups(&input);

    println!("{} groups.", groups.len());
    println!("{} questions answered by someone.", questions_yes_for_any(&groups));
    println!("{} questions answered by everyone.", questions_yes_for_all(&groups));

    return Ok(());
}
