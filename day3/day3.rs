use std::io;
use std::fs::File;
use std::io::Read;

#[derive(Debug)]
struct Map {
    width: usize,
    height: usize,
    lines: Vec<Vec<u8>>,
    //lines: &[&[i8]],
}

impl Map {
    fn is_tree(&self, x: usize, y: usize) -> bool {
        return self.lines[y][x % self.width] == 1;
    }

    fn walk_and_count_trees(&self, dx: usize, dy: usize) -> i32 {
        let mut x: usize = 0;
        let mut y: usize = 0;
        let mut trees: i32 = 0;
        loop {
            x += dx;
            y += dy;
            if y >= self.height {
                break
            }
            if self.is_tree(x, y) {
                trees += 1;
            }
        }
        return trees;
    }
}

fn load_map(mut file: File) -> Result<Map, io::Error> {
    let mut buf = String::new();
    file.read_to_string(&mut buf)?;

    let mut width = 0;
    let mut current_line: Vec<u8>;
    let mut lines: Vec<Vec<u8>> = Vec::new();
    for line in buf.lines() {
        if width == 0 {
            width = line.len();
        } else {
            if width != line.len() {
                panic!("Line width doesn't match");
            }
        }
        current_line = line.chars().map(|c| -> u8 { if c == '.' { 0 } else { 1 }}).collect();
        lines.push(current_line);
    }

    return Ok(Map {
        width: width,
        height: lines.len(),
        lines: lines,
        //lines: &[&[0,1],&[0,0]],
    });
}

fn main() {
    let map = load_map(File::open("input").unwrap()).unwrap();

    //println!("{:?}", map.is_tree(31,0));
    let slopes = [
        (1,1), (3,1), (5,1), (7,1), (1,2)
    ];
    let mut tree_counts : Vec<i64> = Vec::new();
    for slope in &slopes {
        println!("Found {} trees for {},{}",
                 map.walk_and_count_trees(slope.0, slope.1),
                 slope.0,
                 slope.1);
        tree_counts.push(map.walk_and_count_trees(slope.0, slope.1).into());
    }

    //let answer = slopes
    //    .map(|slope| map.walk_and_count_trees(slope.0, slope.1))
    //    .product();
    let count: i64 = tree_counts.iter().product();
    println!("Answer: {}", count);
}
